import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  assignmentNumber: number = null;
  sectionNumber: number = null;
  lectureNumber: number = null;
}
